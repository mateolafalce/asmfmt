/**************************************************************************
 *   main.rs  --  This file is part of asmfmt.                            *
 *                                                                        *
 *   Copyright (C) 2024 Mateo Lafalce                                     *
 *                                                                        *
 *   asmfmt is free software: you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published    *
 *   by the Free Software Foundation, either version 3 of the License,    *
 *   or (at your option) any later version.                               *
 *                                                                        *
 *   asmfmt is distributed in the hope that it will be useful,            *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty          *
 *   of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.              *
 *   See the GNU General Public License for more details.                 *
 *                                                                        *
 *   You should have received a copy of the GNU General Public License    *
 *   along with this program.  If not, see http://www.gnu.org/licenses/.  *
 *                                                                        *
 **************************************************************************/

use std::{
    env::{self, current_dir},
    fs::{self, read_to_string},
    io,
    process::{exit, Command, ExitStatus},
};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    let arg_len: usize = args.len();
    if arg_len <= 1 {
        println!("Usage: asmfmt <file.asm>");
        exit(1);
    }
    if !args.is_empty() && &args[1] == "update" {
        update();
    }
    let directory: &String = &current_dir()?.display().to_string();
    let file: &String = &args[1];
    let path = directory.to_owned() + "/" + file;
    let _ = process_file(path);
    Ok(())
}

#[allow(clippy::needless_range_loop)]
fn process_file(path: String) -> io::Result<()> {
    let file = read_to_string(&path);
    match file {
        Ok(content) => {
            let mut fixed_file = [].to_vec();
            let mut max_len_of_the_line: usize = 0;
            for line in content.lines() {
                fixed_file.push(line.to_string() + "\n");
            }
            // dedicated for newline syntax && tabulations
            for i in 0..fixed_file.len() {
                // convert tab into "    "
                if fixed_file[i].contains('\t') {
                    fixed_file[i] = fixed_file[i].replace('\t', "    ");
                }
                // add 2 newlines up a main label
                if fixed_file[i].contains(':')
                    && !fixed_file[i].starts_with(' ')
                    && i >= 1
                    && fixed_file[i - 1].len() > 1
                    && !fixed_file[i - 1].contains(';')
                {
                    fixed_file[i - 1].push('\n');
                    fixed_file[i - 1].push('\n');
                }
                // add a newline in a main label
                if fixed_file[i].contains(':')
                    && i < fixed_file.len()
                    && !fixed_file[i].starts_with(' ')
                    && !fixed_file[i].starts_with(';')
                    && fixed_file[i + 1].len() > 1
                {
                    fixed_file[i].push('\n');
                }
                // fix when a line is under 4 spaces
                if fixed_file[i].starts_with(' ') {
                    while !fixed_file[i].starts_with("    ") {
                        fixed_file[i].insert(0, ' ')
                    }
                }
                // fix when a line is over 4 spaces
                if fixed_file[i].starts_with("     ") {
                    while fixed_file[i].starts_with("     ") {
                        fixed_file[i].remove(0);
                    }
                }
                // inform that you did not comment on an instruction
                if fixed_file[i].starts_with(' ')
                    && !fixed_file[i].contains(';')
                    && !fixed_file[i].contains(':')
                    && !fixed_file[i].contains("call")
                    && !fixed_file[i].contains("ret")
                    && !fixed_file[i].contains("syscall")
                    && !fixed_file[i].contains("db")
                    && !fixed_file[i].contains("dw")
                    && !fixed_file[i].contains("dd")
                    && !fixed_file[i].contains("dq")
                {
                    println!("Line [{}] doesn't have a comment", i + 1);
                }
                // take the mayor number of a line, without the comment (it's for comments fmt)
                if let Some(index) = fixed_file[i].find(';') {
                    let line = &fixed_file[i][0..index];
                    let mut instruction = line.to_string();
                    while instruction.ends_with(' ') {
                        instruction.truncate(instruction.len() - 1);
                    }
                    if instruction.len() > max_len_of_the_line {
                        max_len_of_the_line = instruction.len();
                    }
                } else {
                    let line = &fixed_file[i];
                    let mut instruction = line.to_string();
                    while instruction.ends_with(' ') {
                        instruction.truncate(instruction.len() - 1);
                    }
                    if instruction.len() > max_len_of_the_line {
                        max_len_of_the_line = instruction.len();
                    }
                }
            }
            // add 4 to the max len of line
            max_len_of_the_line += 4;
            // dedicated for comments
            for i in 0..fixed_file.len() {
                if fixed_file[i].starts_with(' ')
                    && fixed_file[i].contains(';')
                    && !fixed_file[i].contains(':')
                {
                    if let Some(index) = fixed_file[i].find(';') {
                        let comment = &fixed_file[i][index + 1..];
                        if let Some(index) = fixed_file[i].find(';') {
                            let line = &fixed_file[i][0..index];
                            let mut instruction = line.to_string();
                            // if is under the line
                            while instruction.len() < max_len_of_the_line {
                                instruction.push(' ');
                            }
                            // if is over the line
                            while instruction.len() > max_len_of_the_line {
                                instruction.pop();
                            }
                            fixed_file[i] = instruction + ";" + comment;
                        }
                    }
                }
            }
            // convert lines into a string & write the file
            let file_content = fixed_file.join("");
            match fs::write(path, file_content) {
                Ok(_) => (),
                Err(e) => eprintln!("Error writing the file: {}", e),
            }
        }
        Err(_) => eprintln!("The file doesnt exist. Usage: asmfmt <file.asm>"),
    }
    Ok(())
}

fn update() {
    let command: &str = "cargo";
    let args: [&str; 2] = ["install", "asmfmt"];
    let result: ExitStatus = Command::new(command)
        .args(args)
        .spawn()
        .expect("Error with cargo install asmfmt")
        .wait()
        .expect("Error waiting the cargo install asmfmt");
    if result.success() {
        exit(1);
    } else {
        eprintln!("Error: {:?}", result.code());
    }
}
