# asmfmt

A formatter designed for programs in assembly language with AT&T syntax. 

## Usage

```bash
asmfmt <file.asm>
```

## Install

```bash
cargo install asmfmt
```

## Update

```bash
asmfmt update
```

### Before format

```asm
global _start

section .rodata
  nl: db 10
  nl_len: equ $ - nl

section .text

_start:

  call new_line
       call exit
new_line:
 mov rax, 1                  ; write(
      mov rdi, 1       ;   STDOUT_FILENO,
  mov rsi, nl                 ;   text,
      mov rdx, 1        ;   sizeof(text)
  syscall                ; );
  ret
exit:
  mov rax, 60              ; exit(
  mov rdi, 0            ;   EXIT_SUCCESS
  syscall      ; );
```

### After format

```asm
global _start

section .rodata
    nl: db 10

section .text

_start:

    call new_line
    call exit


new_line:

    mov rax, 1             ; write(
    mov rdi, 1             ;   STDOUT_FILENO,
    mov rsi, nl            ;   text,
    mov rdx, 1             ;   sizeof(text)
    syscall                ; );
    ret


exit:

    mov rax, 60            ; exit(
    mov rdi, 0             ;   EXIT_SUCCESS
    syscall                ; );
```